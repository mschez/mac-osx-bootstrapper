setopt prompt_subst

LIGHT_CYAN=$'\e[50m'

# VCS
YS_VCS_PROMPT_PREFIX1=" %{$fg[yellow]%}("
YS_VCS_PROMPT_SUFFIX=""
YS_VCS_PROMPT_DIRTY=") %{$reset_color%}%{$fg[red]%}●"
YS_VCS_PROMPT_CLEAN=") %{$reset_color%}%{$fg[green]%}●"

# Git info
local git_info='$(git_prompt_info)'
ZSH_THEME_GIT_PROMPT_PREFIX="${YS_VCS_PROMPT_PREFIX1}"
ZSH_THEME_GIT_PROMPT_SUFFIX="$YS_VCS_PROMPT_SUFFIX"
ZSH_THEME_GIT_PROMPT_DIRTY="$YS_VCS_PROMPT_DIRTY"
ZSH_THEME_GIT_PROMPT_CLEAN="$YS_VCS_PROMPT_CLEAN"

local exit_code="%(?,,C:%{$fg[red]%}%? %{$reset_color%})"

local sp='$(shrink_path -f)'

# Prompt format:
#
# DATETIME - PRIVILEGES USER @ MACHINE DIRECTORY (BRANCH) STATE C:LAST_EXIT_CODE
# $ COMMAND
#
# For example:
#
# 13:26:52 - mschez @ Maboo ~/.oh-my-zsh (master) ✔️ C:0 $
PROMPT="\
%{$fg[white]%}%* - \
%(#,%{$bg[cyan]%}%{$fg[black]%}%B%n%b:%{$reset_color%},%{$fg[cyan]%}%B%n%b:) \
%{$fg[green]%}@%m \
%{$terminfo[bold]$LIGHT_CYAN%}${sp}%{$reset_color%}\
${git_info} \
%{$fg[white]%}$exit_code%{$fg[white]%}$ %{$reset_color%}"
