#!/usr/bin/env bash
echo "Mac OSX Bootstrapper"
echo "Starting bootstrapping"

# Enable Command Line Tools
xcode-select --install

BOOTSTRAPPER="https://gitlab.com/mschez/mac-osx-bootstrapper/-/raw/master/"

echo "Creating basic folder structure..."
[[ ! -d $HOME/Development ]] && mkdir $HOME/Development
[[ ! -d $HOME/Development/Resources ]] && mkdir $HOME/Development/Resources
[[ ! -d $HOME/Development/Projects ]] && mkdir $HOME/Development/Projects
[[ ! -d $HOME/Development/Credentials ]] && mkdir $HOME/Development/Credentials

echo "Configuring shell..."

# Install oh-my-zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"

# Copy .zshrc
curl -o $HOME/.zshrc -LJO $BOOTSTRAPPER/zsh/.zshrc

# Copy custom theme
curl -o $ZSH_CUSTOM/themes/old-fisherman.zsh-theme -LJO $BOOTSTRAPPER/zsh/old-fisherman.zsh-theme
# Clone Fonts
git clone https://github.com/powerline/fonts.git --depth=1
# Install them
cd fonts
./install.sh
cd ..
rm -rf fonts

# Copy custom plugins
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM}/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM}/plugins/zsh-syntax-highlighting
git clone https://github.com/zsh-users/zsh-history-substring-search.git ${ZSH_CUSTOM}/plugins/zsh-history-substring-search

# Disable insegure directories to autocompletion
compaudit | xargs chmod g-w,o-w

# Check for Homebrew, install if we don't have it
if test ! $(which brew); then
  echo "Installing homebrew..."
  ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
fi

# Update Brew
brew update
brew tap adoptopenjdk/openjdk

echo "Installing packages..."
BREW_PACKAGES=(
  exiftool
  ffmpeg
  imagemagick
  jq
  mas
  nvm
  postgresql
  pygments
  python
  python3
  rename
  rust
  tree
  vim
  yarn
  watchman
  wget
  zsh-autosuggestions
  zsh-syntax-highlighting
  zsh-history-substring-search
)

brew install ${BREW_PACKAGES[@]}

echo "Configuring packages..."

# nvm
[[ ! -d $HOME/.nvm ]] && mkdir $HOME/.nvm
export NVM_DIR="$HOME/.nvm"

echo "Installing apps from Cask..."
BREW_CASKS=(
  1password
  1clipboard
  ableton-live-suite
  adoptopenjdk
  aerial
  android-platform-tools
  android-sdk
  android-studio
  appcleaner
  chrome-remote-desktop-host
  dexed
  docker
  eul
  firefox
  fork
  google-chrome
  google-drive
  grammarly
  grandperspective
  handbrake
  imageoptim
  iterm2
  keybase
  localtunnel
  macvim
  macpass
  native-access
  plex
  postman
  react-native-debugger
  rectangle
  responsively
  robo-3t
  sequel-ace
  signal
  slack
  sublime-text
  the-unarchiver
  trello
  visual-studio-code
  vlc
  whatsapp
  yed
)

brew install --cask ${BREW_CASKS[@]}

echo "Installing apps from Mac App Store..."
# Install Trello
mas install 1278508951
# Install Gifski
mas install 1351639930

# Missing apps
echo "Some still missing apps..."
echo "Arturia Software Center: http://downloads.arturia.net/infrastructure/asc/soft/Arturia_Software_Center_2_0_3_1171.pkg"

brew cleanup

echo "Configuring apps..."
# Configure VSCODE
# Import settings.json
[[ ! -d $HOME/Library/Application\ Support/Code/ ]] && mkdir $HOME/Library/Application\ Support/Code/
[[ ! -d $HOME/Library/Application\ Support/Code/User ]] && mkdir $HOME/Library/Application\ Support/Code/User
curl -o $HOME/Library/Application\ Support/Code/User/settings.json -LJO $BOOTSTRAPPER/vscode/settings.json
curl -o $HOME/Library/Application\ Support/Code/User/keybindings.json -LJO $BOOTSTRAPPER/vscode/keybindings.json
curl -o $HOME/Library/Fonts/DankMono-Italic.ttf -LJO $BOOTSTRAPPER/vscode/DankMono-Italic.ttf
curl -o $HOME/Library/Fonts/DankMono-Regular.ttf -LJO $BOOTSTRAPPER/vscode/DankMono-Regular.ttf

# Configure GPMDP
git clone https://github.com/ronilaukkarinen/gpmdp-dark-theme.git $HOME/Development/Resources/gpmdp-dark-theme

# Configure iTerm2
curl -o $HOME/Library/Application Support/iTerm2/DynamicProfiles/Profiles.json -LJO $BOOTSTRAPPER/iterm2/Profiles.json

# Install extensions
VSCODE_EXTENSIONS=(
  dbaeumer.vscode-eslint
  dracula-theme.theme-dracula
  dsznajder.es7-react-js-snippets
  eamodio.gitlens
  ms-vscode.sublime-keybindings
  msjsdiag.vscode-react-native
  rust-lang.rust
  shardulm94.trailing-spaces
  tyriar.sort-lines
  stevencl.adddoccomments
)

for extension in "${VSCODE_EXTENSIONS[@]}"
do
  code --install-extension $extension --force
done

# Configure system defaults
echo "Configuring OSX..."

# Key delays
defaults -currentHost write NSGlobalDomain KeyRepeat -int 2
defaults -currentHost write NSGlobalDomain InitialKeyRepeat -int -15

# Require password as soon as screensaver or sleep mode starts
defaults -currentHost write com.apple.screensaver askForPassword -int 1
defaults -currentHost write com.apple.screensaver askForPasswordDelay -int 0
defaults -currentHost write com.apple.screensaver moduleDict -dict moduleName Aerial path $HOME/Library/Screen\ Savers/Aerial.saver/ type 0

# TODO
# set hide dock automatically
# set active corners
# set finder status bar visible
# set finder as columns
# set finder Downloads as main folder

echo "Time to manual configuration ¯\_(ツ)_/¯"

# NAS folders
NAS_IP=$(curl -s "https://dec.quickconnect.to/finder/get.php" | jq -r ".[0].ipv4_interface | .[0]")
NAS_FOLDERS=(
  backup
  downloads
  home
  photo
  video
)
echo "Create NAS folders and symlinks"
echo "open ${NAS_FOLDERS[@]/#/$NAS_IP/}"
echo "ln -s /Volumes/downloads/ $HOME/Downloads/Sarasvati"
echo "ln -s /Volumes/home/ $HOME/Sarasvati"
echo "ln -s /Volumes/photo/ $HOME/Pictures/Sarasvati"
echo "ln -s /Volumes/video/ $HOME/Movies/Sarasvati"

# Ableton VST
echo "Downloading basic free VST"
curl -o $HOME/Downloads/dexed-0.9.4-osx.mpkg -LJO $BOOTSTRAPPER/ableton/dexed-0.9.4-osx.mpkg
curl -o $HOME/Downloads/Helm_v0_9_0_r.pkg -LJO $BOOTSTRAPPER/ableton/Helm_v0_9_0_r.pkg
curl -o $HOME/Downloads/TyrellN6_3.0_Installer -LJO $BOOTSTRAPPER/ableton/TyrellN6_3.0_Installer

open $HOME/Downloads/Helm_v0_9_0_r.pkg
open $HOME/Downloads/TyrellN6_3.0_Installer